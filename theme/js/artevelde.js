(function ($, Drupal) {
  Drupal.behaviors.artevelde = {
    attach: function (context, settings) {
      if (context !== document) return;

      Vue.component('step', {
        template: '#step',
        props: ['step'],
        methods: {
          // Returns the step ID
          getStepId: function () {
            return 'Step '+ this.step;
          },

          // Returns a random colour for the element
          getBackground: function() {
            var colour = ['yellow', 'lightgreen', 'lightblue', 'white', 'orange'][Math.floor(Math.random() * 5)];

            return {
              'background-color': colour,
            }
          }
        },
      });

      // Main Vue component
      Drupal.behaviors.artevelde.vm = new Vue({
        el: '#stepslist',
        data: function () {
          // Data from the module is passed via drupalSettings to the JS
          // It can now be manipulated in the browser
          return {
            startCount: drupalSettings.artevelde.startCount,
            step: drupalSettings.artevelde.step,
            max: drupalSettings.artevelde.max,
            steps: [], // Populated with JS
          }
        },
        methods: {
          // Calculate the steps
          getSteps: function() {
            var steps = [];

            for (i = this.startCount; i <= this.max; i += this.step) {
              steps.push(i);
            }

            return steps;
          },
        }
      });
    },
  }
})(jQuery, Drupal);
