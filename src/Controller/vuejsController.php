<?php

namespace Drupal\vuejs_demo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class vuejsController.
 *
 * @package Drupal\vuejs_demo\Controller
 */
class vuejsController extends ControllerBase {

  /**
   * Page Controller
   *
   * @return array $build
   */
  public function page() {

    $startCount = 0;
    $step = 2;
    $max = 50;

    $build = [
      '#theme' => 'vuejs_demo_page',
      '#title' => $this->getTitle(),
      '#attached' => [
        'library' => [
          'vuejs_demo/artevelde',
        ],
        'drupalSettings' => [
          'artevelde' => [
            'startCount' => $startCount,
            'step' => $step,
            'max' => $max,
          ],
        ],
      ],
    ];

    return $build;
  }

  /**
   * Returns the title for the page
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getTitle() {
    return t('Welcome to my VueJS demo');
  }
}